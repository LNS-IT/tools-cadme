# export CADDY_VERSION=2.7.4
# login to aws ecr!
# docker buildx create --use
# docker buildx build --no-cache --build-arg CADDY_VERSION=$CADDY_VERSION --platform linux/arm64,linux/amd64 --push -t public.ecr.aws/unocha/caddy:$CADDY_VERSION .

ARG TARGETPLATFORM
ARG TARGETOS
ARG TARGETARCH
ARG CADDY_VERSION=2.7.4

FROM caddy:${CADDY_VERSION}-builder AS builder

RUN CGO_ENABLED=0 GOARCH=${TARGETARCH} GOOS=${TARGETOS} \
    xcaddy build \
    --with github.com/greenpau/caddy-security \
    --with github.com/lucaslorentz/caddy-docker-proxy/v2

FROM caddy:${CADDY_VERSION}-alpine

COPY --from=builder /usr/bin/caddy /usr/bin/caddy

RUN apk add nss-tools curl && \
    rm -rf /var/cache/apk/*

CMD ["caddy", "docker-proxy", "--caddyfile-path", "/etc/caddy/Caddyfile"]

# to be able to tunnel into the admin api to grab profiles
EXPOSE 12019
